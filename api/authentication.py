"""Authentication classes for Django Rest Framework.
Classes:
    ExpiringTokenAuthentication: Authentication using extended authtoken model.
"""

from rest_framework import exceptions
from rest_framework.authentication import TokenAuthentication
from rest_framework import permissions

from django.utils import timezone
import datetime

from .models import Token
from expensify.settings import EXPIRING_TOKEN_LIFESPAN

class ExpiryTokenAuthentication(TokenAuthentication):
	models = Token
	
	def authenticate_credentials(self, key):
	        """Attempt token authentication using the provided key."""
	        try:
	            token = self.models.objects.get(key=key)
	        except self.model.DoesNotExist:
	            raise exceptions.AuthenticationFailed('Invalid token')
	        if not token.user.is_active:
	            raise exceptions.AuthenticationFailed('User inactive or deleted')
	        now = timezone.now()
		if token.created < now - EXPIRING_TOKEN_LIFESPAN:
	             raise exceptions.AuthenticationFailed('Token has expired')
	    

class IsOwnerOrReadOnly(permissions.BasePermission):

    def has_object_permission(self, request, view, obj):
        if request.method in permissions.SAFE_METHODS:
            return True
        return obj.owner == request.user



from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.models import User
from django.contrib import auth 
from django.utils import timezone

from rest_framework import generics
from rest_framework.response import Response
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework.authentication import TokenAuthentication

from .models import Api
from .models import Token
from .serializers import ApiSerializer
from .authentication import IsOwnerOrReadOnly, ExpiryTokenAuthentication
from expensify.settings import EXPIRING_TOKEN_LIFESPAN

import datetime


class ApiList(generics.ListCreateAPIView):
    queryset = Api.objects.all()
    serializer_class = ApiSerializer

    authentication_classes = (ExpiryTokenAuthentication,TokenAuthentication)
    permission_classes = (IsAuthenticated, IsOwnerOrReadOnly)

    def get_queryset(self):
        user = self.request.user
        return Api.objects.filter(user=user)
		
    def perform_create(self, serializer):
	instance = serializer.save()
	return Response({'status': status.HTTP_201_CREATED })

class ApiListDetail(generics.RetrieveUpdateDestroyAPIView):
    authentication_classes = (ExpiryTokenAuthentication,TokenAuthentication)
    permission_classes = (IsAuthenticated, IsOwnerOrReadOnly)
    queryset = Api.objects.all()
    serializer_class = ApiSerializer

    def get_queryset(self):
        user = self.request.user
        return Api.objects.filter(user=user)



@csrf_exempt
def token(request):
	user = auth.authenticate(username=request.POST.get('user'), password=request.POST.get('password'))
	if user is not None:
		token = Token.objects.get_or_create(user=user)
		if token[0].created < timezone.now() - EXPIRING_TOKEN_LIFESPAN:
			token.delete()
			Token.objects.create(user=user)
		return HttpResponse(token[0].key)
	return HttpResponse("Invalid Credentials")






#curl -X POST -d "user=kush&password=q"  http://localhost:8000/api/token/

#6cc32266177605184253ceb94497afe03aff84b9

#curl -X POST http://localhost:8000/test/ -d {'user': kushagra}

# http --form -a kushagra:q POST http://127.0.0.1:8000/api/ user="1" price="3" date="3"

# http -a kushagra:q http://127.0.0.1:8000/api/

#http --form -h 'Authorization: Token 6cc32266177605184253ceb94497afe03aff84b9' POST http://127.0.0.1:8000/api/ user="1" price="71" date="71"

#curl -X POST -d "user=Pepe&password=aaaa"  http://localhost:8000/
#{"token": "f7d6d027025c828b65cee5d38240aec60dffa150", "detail": "POST answer"}%

 #curl http://localhost:8000/api/ -H 'Authorization: Token ada1ca24066b3e07918822f085b7e6d94313a345'
#{"user": "kushagra" ,"price" : "12" , "date" : "123"}

#curl -u kushagra:q http://127.0.0.1:8000/api/

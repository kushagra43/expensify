from django.db import models
from django.contrib.auth.models import User
# Create your models here.
from django.conf import settings
from rest_framework.authtoken.models import Token

class Api(models.Model):
    user = models.ForeignKey(User)
    merchant_name = models.CharField(max_length=200, verbose_name = 'name of the merchant')
    price = models.DecimalField(max_digits=9, decimal_places=6, verbose_name = 'product price')
    date = models.CharField(max_length=200, verbose_name = 'date of recipt')
    recipt = models.ImageField(upload_to='static/')	
    def __str__(self):
        return str(self.id)
    class Meta:
            db_table = 'Api'
            verbose_name = 'Api'
            verbose_name_plural = 'Apis'



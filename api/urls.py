from django.conf.urls import patterns, url
from rest_framework.urlpatterns import format_suffix_patterns
from api import views
from django.contrib import staticfiles
from api import helper
from django.core.urlresolvers import reverse_lazy
from django.views.generic import TemplateView , RedirectView

urlpatterns = patterns('',
    url(r'^$', views.ApiList.as_view() , name='home'),
    url(r'^(?P<pk>[0-9]+)/$',
    views.ApiListDetail.as_view()),
    url(r'^static/(?P<path>.*)$','api.helper.serve'),
    url(r'^test$','api.helper.test'),
    url(r'^token/$','api.views.token'),		


    )

urlpatterns = format_suffix_patterns(urlpatterns)
